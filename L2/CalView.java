import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.IOException; 
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner ; 
import com.google.gson.JsonArray ;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CalView {
	private static final String redirURI = "http://www.cs.drexel.edu" ;
	private static final String oauthURL = "https://accounts.google.com/o/oauth2/token" ; 

	
	public static String executePost( String targetURL, String urlParams) throws Exception{
		
		URL url ; 
		HttpURLConnection connection = null; 
		try{
			url = new URL(targetURL) ; 
			connection = (HttpURLConnection)url.openConnection() ; 
			connection.setRequestMethod("POST") ; 
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			//connection.setRequestProperty("Content-Length", ""+ Integer.toString(urlParams.getBytes().length));
			connection.setRequestProperty("Content-Length", ""+urlParams.length());
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream( connection.getOutputStream()); 
			wr.writeBytes(urlParams);
			wr.flush();
			wr.close(); 
			InputStream iS = null ; 
			try{
				iS = connection.getInputStream();
			}catch(IOException e){
				return null; 
			}
			BufferedReader rD = new BufferedReader(new InputStreamReader(iS)) ; 
			String line ; 
			StringBuffer response = new StringBuffer() ; 
			while((line = rD.readLine())!= null){
				response.append(line) ; 
				response.append('\r') ; 
			}
			rD.close();
			return response.toString(); 
			
		}catch(Exception e ){
			e.printStackTrace() ; 
			return null ; 
			
		}finally{
			
			if( connection != null){
				connection.disconnect(); 
			}
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		System.out.println(""); 
		System.out.println("                    CalView                  ");
		System.out.println("           See some Upcoming Events          ");
		System.out.println("   By Brandon Rupert <barupert@gmail.com>    ");
		System.out.println("   A Google Calendar Client for CS 275 L2    ");
		System.out.println("");
		
		String cliID = "" ; //need client id for authentication; set from prompt 
		Scanner user = new Scanner(System.in) ; 
		System.out.print("Enter client ID : ");
		cliID = user.nextLine(); 
		String cliSecret = "" ; //need this for authorization params; set from prompt
		System.out.print("Enter client secret : ");
		cliSecret = user.nextLine(); 
		System.out.println("Accessing oauth access token...");
		
		String allowCode = ""; //will allow user a token
		String allowURL = "https://accounts.google.com/o/oauth2/auth?" ; //Construct allowURL, go there & get allow code
		allowURL += "&access_type=offline" ; 
		allowURL += "&client_id=" + cliID  ; 
		allowURL += "&scope=https://www.googleapis.com/auth/calendar" ; 
		allowURL += "&response_type=code" ; 
		allowURL += "&redirect_uri=" + redirURI ; 
		allowURL += "&state=/profile" ; 
		allowURL += "&approval_prompt=force"; 
		System.out.println("Go to the following for allow code; follow directions there;");
		System.out.println("Ultimately look in the browser's URL field for"); 
		System.out.println("some section like code=, and so forth; might have to click the field"); 
		System.out.println("To see whole URL; the character # at the end of the code is not part of the code"); 
		System.out.println(allowURL);
		System.out.print("Enter allow code : ") ; 
		allowCode = user.nextLine(); 
		String authoParams = "code=" + allowCode ; 
		authoParams += "&client_id=" + cliID ; 
		authoParams += "&client_secret=" + cliSecret ; 
		authoParams += "&redirect_uri=" + redirURI ; 
		authoParams += "&grant_type=authorization_code" ; 
		String authoResponse =""; 
		try{
			authoResponse = executePost( oauthURL, authoParams);
		}catch(Exception e){
			System.out.println("Could not post. Exiting");
		}
		//access authorization key 
		String accessToken ; 
		JsonParser oauthJP = new JsonParser() ; 
		JsonElement oauthRoot = null ;
		try{
			oauthRoot = oauthJP.parse(authoResponse) ; 
		}catch(Exception e){
			System.out.println("Connection error. Check input. Exiting");
			return ; 
		}
		JsonObject oauthRootObj = oauthRoot.getAsJsonObject(); 
		//oauthRootObj might be valid...
		try{
			//... if not we can't get stuff from it
			accessToken = oauthRootObj.get("access_token").getAsString();
		
		}catch( Exception e){
			System.out.println("Could not get access token. Exiting. Check input");
			return ; 
		}
		System.out.println("Got oauth access token... ");
		System.out.println("Accessing calendars...");
		String strURL = "https://www.googleapis.com/calendar/v3/users/me/calendarList?access_token=" + accessToken;
		
		RootJsonObj rJ = new RootJsonObj(strURL);
		JsonObject jO = rJ.getJsonObject(); 
	
	
		rJ = new RootJsonObj(strURL); 
		jO = rJ.getJsonObject(); 
		JsonArray allCalsItems; 
		try{
			allCalsItems = jO.get("items").getAsJsonArray();
		}catch(Exception e){
			
			System.out.println("Could not access calendars. Exiting");
			return ; 
		}
		JsonObject firstCal = allCalsItems.get(0).getAsJsonObject();
		System.out.println("===");
		System.out.println("You have the following calendars...");
		for(JsonElement eachCal : allCalsItems ){
			
			System.out.println(eachCal.getAsJsonObject().get("summary").getAsString());
		}
		String firstCalID = firstCal.get("id").getAsString()  ; 
		strURL = "https://www.googleapis.com/calendar/v3/calendars/"+firstCalID+"/events?access_token="+accessToken;
		rJ = new RootJsonObj(strURL) ; 
		jO = rJ.getJsonObject(); 
		JsonObject allEventsOverall = jO.getAsJsonObject() ;
		JsonArray allEvents = null ; 
		try{
			allEvents = allEventsOverall.get("items").getAsJsonArray();
		}catch(Exception e){
			
			System.out.println("Could not access events. Exiting");
			return ; 
		}	
		String calTitle = allEventsOverall.get("summary").getAsString();
		JsonObject eventObj;
		JsonObject eventStart;
		JsonObject eventEnd ;
		String eventName ;
		System.out.println("===");
		System.out.println("In "+ calTitle + " you have noted ...") ; 
		for( JsonElement eachEvent: allEvents){
			eventObj = eachEvent.getAsJsonObject(); 
			eventName = eventObj.get("summary").getAsString();
			eventStart = eventObj.get("start").getAsJsonObject(); 
			eventEnd = eventObj.get("end").getAsJsonObject(); 

			System.out.print(  "-> " +eventName + " from ");
			try{ 
				System.out.print(  eventStart.get("dateTime").getAsString()  + " ");
			}
			catch( NullPointerException e ){
				System.out.print( "[no datetime]");
			}
			System.out.print( " to ");
			try{ 
				System.out.print( eventEnd.get("dateTime").getAsString() +"\n");
				
			}catch(NullPointerException e){
				System.out.print("[no datetime]\n");
			}
			
		}
	}


}
