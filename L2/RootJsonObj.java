import java.io.InputStream; 
import java.io.InputStreamReader ; 
import java.net.HttpURLConnection ; 
import java.net.URL ;  
import com.google.gson.JsonElement  ; 
import com.google.gson.JsonObject; 
import com.google.gson.JsonParser ;


/**
 * Top level  JSON object in hierarchy at some URL
 * 
 * @author Brandon Rupert
 * @version 1.0
 */
public class RootJsonObj {
	private static int nConnects = 0 ; 
	private URL url ; 
	private HttpURLConnection request ; 
	private JsonParser jp ;
	JsonElement root ;
	/**
	 * Initialize RootJsonObj
	 * @param urlIn String
	 * @throws Exception
	 */
	public RootJsonObj( String urlIn) throws Exception{
		
		url = new URL( urlIn) ; 
		request = (HttpURLConnection) url.openConnection();
		request.connect(); 
		
		
		
		jp = new JsonParser()  ;
		root = jp.parse( new InputStreamReader((InputStream)
				request.getContent())) ;
		
		nConnects++ ; 
		request.disconnect() ; 
				
	}
	/**
	 * 
	 * @return JsonObject
	 */
	public JsonObject getJsonObject(){
		return root.getAsJsonObject() ;
	}
	/**
	 * Counts successful connections in current
	 * running
	 * program
	 * 
	 * @return int
	 */
	public static int nConnectsInProg( ){
		
		return nConnects ; 
	}
}

