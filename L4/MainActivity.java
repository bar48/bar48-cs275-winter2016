package com.example.weatheragain;

import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;




public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//ImageView weatherPicture = (ImageView) findViewById(R.id.weatherPicture) ; 
		//weatherPicture.setImageResource(R.drawable.ic_launcher);
		new GetForecastTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private class GetForecastTask extends AsyncTask<Void,Void,Void>{
		
		private  String url ;
		private String dbName; 
		private String[] results ;  
		long timeAccessedSeconds ; 
		String key = "491c70e23487d54d"; 

		public ForecastResult[] useWunderground( String key ) throws Exception{
			String lookupURL = "http://api.wunderground.com/api/"+key+"/geolookup/q/autoip.json" ; 
			RootJsonObj ro = new RootJsonObj( lookupURL) ; 
			JsonObject loc = null ; 
			try {
				loc = ro.getJsonObject().get("location").getAsJsonObject() ; 
			}
			catch( NullPointerException e){ 
				return null ; 
			}
			String cityName, stateAbbrev ; 
			cityName = loc.get("city").getAsString(); 
			stateAbbrev = loc.get("state").getAsString(); 
			
			//For query of hourly weather forecast 
			String hourlyURL = "http://api.wunderground.com/api/"+key+"/hourly/q/"+stateAbbrev+"/"
					+cityName +".json" ; 
			ro = new RootJsonObj(hourlyURL) ; 

			//rootObj = root.getAsJsonObject(); 
			JsonArray hourlyFct = null ; 
			try{
				hourlyFct = ro.getJsonObject().get("hourly_forecast").getAsJsonArray();
			}
			catch( NullPointerException e){
				
				return null ; 
			}
			//get weather forecast reseults
			String[] listRows = new String[hourlyFct.size()] ;
			ForecastResult[] allResults = new ForecastResult[hourlyFct.size()]; 
			JsonObject hour = null ;
			JsonObject time = null ; 
			String entry = null ;
			ForecastResult fc = null ; 
			for(int i =0; i < hourlyFct.size(); i++){
				hour= hourlyFct.get(i).getAsJsonObject();
				time = hour.get("FCTTIME").getAsJsonObject(); 
				fc = new ForecastResult( time.get("pretty").getAsString(),  
						time.get("weekday_name").getAsString(), 
						hour.get("condition").getAsString(), 
						hour.get("temp").getAsJsonObject().get("english").getAsString(), 
						hour.get("humidity").getAsString(), 
						hour.get("icon_url").getAsString(),
						timeAccessedSeconds) ; 
				allResults[i] = fc ; 
				
			}
			
			
			
			return allResults ; 
		}
		
		@Override
		
		protected Void doInBackground(Void... params) {
			ForecastResult[] results; 
			try {
				 results = useWunderground( "491c70e23487d54d") ;
				 String[] resultsStr = new String[results.length] ; 
				 for(int i=0; i < results.length; i++){
					 
					 resultsStr[i] = results[i].getAsLine(); 
				 }
				 Context context = MainActivity.this;
				 ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,resultsStr);
				 ListView listView = (ListView) findViewById( R.id.listView1);
				 listView.setAdapter(itemsAdapter);
				 String iconURL = results[0].getIconURL(); 
				 URL newURL = new URL(iconURL); 
				 Bitmap iconIn = BitmapFactory.decodeStream(newURL.openConnection().getInputStream());
				 ImageView weatherPicture = (ImageView) findViewById(R.id.weatherPicture) ; 
				 weatherPicture.setImageBitmap(iconIn);			 
				 
				 
			} catch (Exception e) {
				return null ; 
			} 
			
			return null;
		}

		
		
		
	}
}





