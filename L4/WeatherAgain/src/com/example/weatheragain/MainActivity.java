package com.example.weatheragain;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;




public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//ImageView weatherPicture = (ImageView) findViewById(R.id.weatherPicture) ; 
		//weatherPicture.setImageResource(R.drawable.ic_launcher);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}


class GetForecastTask extends AsyncTask<Void, Void,Void>{
	
	private  String url ;
	private String dbName; 
	private String[] results ;  
	public GetForecastTask( String url, String dbName){
		
		this.url = url ; 
		//"http://api.wunderground.com/api/"+"491c70e23487d54d" +"/geolookup/q/autoip.json"
		this.dbName = dbName ; 

	}
	private ForecastResult[] useDatabase(String dbName){
		
		return null ; 
	}
	public ForecastResult[] useWunderground( String key ) throws Exception{
		String lookupURL = "http://api.wunderground.com/api/"+"491c70e23487d54d" +"/geolookup/q/autoip.json" ; 
		RootJsonObj ro = new RootJsonObj( lookupURL) ; 
		JsonObject loc = null ; 
		try {
			loc = ro.getJsonObject().get("location").getAsJsonObject() ; 
		}
		catch( NullPointerException e){ 
			return null ; 
		}
		String cityName, stateAbbrev ; 
		cityName = loc.get("city").getAsString(); 
		stateAbbrev = loc.get("state").getAsString(); 
		
		//For query of hourly weather forecast 
		String hourlyURL = "http://api.wunderground.com/api/"+key+"/hourly/q/"+stateAbbrev+"/"
				+cityName +".json" ; 
		ro = new RootJsonObj(hourlyURL) ; 

		//rootObj = root.getAsJsonObject(); 
		JsonArray hourlyFct = null ; 
		try{
			hourlyFct = ro.getJsonObject().get("hourly_forecast").getAsJsonArray();
		}
		catch( NullPointerException e){
			
			return null ; 
		}
		
		String[] listRows = new String[hourlyFct.size()] ;
		ForecastResult[] allResults = new ForecastResult[hourlyFct.size()]; 
		JsonObject hour = null ;
		JsonObject time = null ; 
		String entry = null ;
		ForecastResult fc = null ; 
		for(int i =0; i < hourlyFct.size(); i++){
			hour= hourlyFct.get(i).getAsJsonObject();
			time = hour.get("FCTTIME").getAsJsonObject(); 
			fc = new ForecastResult( time.get("pretty").getAsString(),  
					time.get("weekday_name").getAsString(), 
					hour.get("condition").getAsString(), 
					hour.get("temp").getAsJsonObject().get("english").getAsString(), 
					hour.get("humidity").getAsString(), 
					hour.get("icon_url").getAsString()) ; 
			allResults[i] = fc ; 
			
		}
		
		
		
		return allResults ; 
	}
	
	@Override
	
	protected Void doInBackground(Void... params) {

		
		
		return null;
	}
	
}

class ForecastResult{
	
	private String prettyTime, weekday, condition, temp, humidity, iconURL ; 
	
	public ForecastResult( String prettyTime, String weekday, String condition, String temp, String humidity, String iconURL){
		
		this.prettyTime = prettyTime ; 
		this.weekday = weekday ; 
		this.condition = condition ; 
		this.temp = temp ; 
		this.humidity = humidity ; 
		this.iconURL = iconURL ; 
	}
	public String getTime( ){
		return prettyTime ; 
	}
	public String getWeekday(){
		return weekday ; 
	}
	public String getCondition(){
		
		return condition ; 
	}
	public String getTemp(){
		return temp ; 
	}
	public String getHumidity(){
		return humidity ; 
	}
	public String getIconURL(){
		return iconURL ; 
	}
	public String getAsLine(){
		
		return "At " + getTime() + " on " + getWeekday() + " expect "  + getCondition() 
			+ " Temp(F) " + getTemp() + " Humidity " + getHumidity() ; 
	}
	
}
