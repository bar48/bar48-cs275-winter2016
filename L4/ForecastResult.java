package com.example.weatheragain;
//To hold Strings, info from wunderground forecast 
//Can easily return individual fields for SQLite use
//or as a line for entry in scrollable list or terminal printout
//long, seconds since epoch ; tell when data was accessed
public class ForecastResult{
	
	private String prettyTime, weekday, condition, temp, humidity, iconURL ; 
	private long seconds ; 
	//Initialize it all at once
	public ForecastResult( String prettyTime, String weekday, String condition, String temp, String humidity, String iconURL, 
			long seconds){
		
		this.prettyTime = prettyTime ; 
		this.weekday = weekday ; 
		this.condition = condition ; 
		this.temp = temp ; 
		this.humidity = humidity ; 
		this.iconURL = iconURL ; 
		this.seconds = seconds ; 
	}
	public String getTime( ){
		return prettyTime ; 
	}
	public String getWeekday(){
		return weekday ; 
	}
	public String getCondition(){
		
		return condition ; 
	}
	public String getTemp(){
		return temp ; 
	}
	public String getHumidity(){
		return humidity ; 
	}
	public String getIconURL(){
		return iconURL ; 
	}
	public long getAccessTimeSeconds(){
		return seconds ; 
	}
	public String getAsLine(){
		
		return "At " + getTime() + " on " + getWeekday() + " expect "  + getCondition() 
			+ " Temp(F) " + getTemp() + " Humidity " + getHumidity() ; 
	}
	
}
