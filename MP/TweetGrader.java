import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooSession;

public class TweetGrader {
	static final int N_TWEETS_PER_USER = 30 ;
	static final int N_POLYSYLLABLE = 3 ; 

	private static final String TEMBOO_ACCT_NAME ="brandonrupert"; 
	private static final String TEMBOO_APP_KEY_NAME = "TweetGrader" ; 
	private static final String TEMBOO_APP_KEY_VAL = "YoBUfD4tRo3cqHTl4A8rws6w2W41anja" ; 
	public static String wordnikHttp( String word, String apiKey){
		String preWord = "http://api.wordnik.com:80/v4/word.json/" ; 
		String postWord = "/hyphenation?useCanonical=false&limit=50&api_key=" ; 
		return preWord + word + postWord + apiKey ; 
	}
	
	public static boolean isValid( String wordnikApi){
		RootJsonObj rJo = null ; 
		try{
			rJo = new RootJsonObj( wordnikHttp("hello", wordnikApi))  ;
		}catch(Exception e){
			return false ; 
		}
		return true ; 
		
	}
	public static double smogGrade( int nPolys, int nSents){
		//defined in http://en.wikipedia.org/wiki/SMOG 
		//and at Midterm Practicum assignment page
		//tweet counts as a sentence
		double coeff1 = 1.0430 ; 
		double term2 = 3.1291 ; 
		double term1 = Math.pow( nPolys*(30.0/nSents)    , 0.5) ; 
		return coeff1*term1 + term2 ;  
		
	}
	public static int countSyllables( String word, String apiKey) throws Exception{

		RootJsonObj rJo ; 
		try{
			rJo = new RootJsonObj( wordnikHttp( word, apiKey ))  ;
		}catch(Exception e){
			return 0; 
		}

		try{
			return rJo.getJsonArray().size(); 
		}catch( Exception e ){
			return 0 ; 
		}
		

		
	}
	//take the Tweets beforehand and make it into a String[] , 
	//each element of String[] a single Tweet
	public static boolean isTweetKeyword( String word ){
		return ( ( word.contains("@")) || (word.contains("#")) || ( word.contains("http"))
						|| word.contains("RT")) ; 
	}
	public static int countPolysSentence( String sentence, String apiKey) throws Exception{
		
		String[] words = sentence.split("\\s+");
		String raw; 
		int nP =0;
		int nS =0 ; 
		if ( words.length  == 0) return nP ; 
		for ( String word : words){
			raw = word.replaceAll("[^A-Za-z]", "").toLowerCase();
			
			if ( ( !raw.equals("")  ) && ( !isTweetKeyword(raw))){
				//System.out.println(raw);
				nS =  countSyllables( raw, apiKey);
				if ( nS >= N_POLYSYLLABLE) nP ++ ;
			}
		}
		return nP;
		
	}
	public static double getGradePassage( String[] passage, String apiKey ) throws Exception{
		
		if ( passage.length == 0 ) return 0.0  ; 
		int nPolys =0; 
		int nSents = passage.length  ;
		for ( int i = 0 ; i < passage.length; i++ ){
			
			nPolys += countPolysSentence( passage[i], apiKey);
		}
		return smogGrade( nPolys, nSents);
		
		
	}

	
	public static void main(String[] args ) throws Exception{
		
		System.out.println("Find SMOG grade of reading level of someone's tweets");
		Scanner user = new Scanner(System.in); 
		System.out.print("Enter Wordnik API key: ");
		String wordnikApi = user.nextLine().trim(); 
		System.out.print("Enter Twitter consumer key : ");
		String twitterConsumerKey = user.nextLine().trim(); 
		System.out.print("Enter Twitter consumer secret : ");
		String twitterConsumerSecret = user.nextLine().trim(); 
		System.out.print("Enter Twitter Access token : ");
		String twitterAccessToken = user.nextLine().trim(); 
		System.out.print("Enter Twitter Access token secret : ");
		String twitterAccessTokenSecret = user.nextLine().trim(); 
		
		if ( !isValid( wordnikApi)){
			System.out.println("Check inputs. Exiting.");
			return ; 
		}
		 
		TembooSession tSession = new TembooSession(TEMBOO_ACCT_NAME,TEMBOO_APP_KEY_NAME, TEMBOO_APP_KEY_VAL);
		UserTimeline ut = new UserTimeline( tSession);
		UserTimelineInputSet uts = ut.newInputSet(); 
		uts.set_AccessToken(twitterAccessToken);
		uts.set_AccessTokenSecret(twitterAccessTokenSecret);
		uts.set_ConsumerKey(twitterConsumerKey);
		uts.set_ConsumerSecret(twitterConsumerSecret);
		uts.set_Count(N_TWEETS_PER_USER);
		System.out.print("Query username (no @) : "); 
		String query = user.nextLine( ).toLowerCase() ; 
		uts.set_ScreenName( query);
		UserTimelineResultSet utrs = null;
		try{
			utrs = ut.execute( uts);
		}catch(Exception e){
			System.out.println("Could not find tweets. Check inputs.");
			return ; 
		}
		
		JsonParser jp = new JsonParser() ; 
		JsonElement root = jp.parse( utrs.get_Response());
		JsonArray ja = root.getAsJsonArray(); 
		if ( ja.size() == 0 ){
			System.out.println("Could not find tweets. Check inputs.");
			return ; 
		}
		String[] tweets = new String[ja.size()] ; 
		for( int i =0; i < ja.size(); i++ ){
			
			tweets[i] = ja.get(i).getAsJsonObject().get("text").getAsString( ); 

		}
		System.out.println("Finding SMOG grade ....");
		double smogGrade = getGradePassage(tweets, wordnikApi) ;
		System.out.println( "SMOG grade : " + smogGrade);

		
			
		
		
	}

}
