import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.IOException; 
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner ; 


public class ExecPost{
	/*
	 *	function from Drexel Bill Mongan's CS 275 slideshow in constructor and in 
	 *  getResponse
	 *	
	 */

	private URL url ; 
	private HttpURLConnection connection = null ; 
	String responseStr = null ; 
	
	public ExecPost( String targetURL, String urlParams){
		try{
			url = new URL(targetURL) ; 
			connection = (HttpURLConnection)url.openConnection() ; 
			connection.setRequestMethod("POST") ; 
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			//connection.setRequestProperty("Content-Length", ""+ Integer.toString(urlParams.getBytes().length));
			connection.setRequestProperty("Content-Length", ""+urlParams.length());
			connection.setRequestProperty("Content-Language", "en-US");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream( connection.getOutputStream()); 
			wr.writeBytes(urlParams);
			wr.flush();
			wr.close(); 
			InputStream iS = null ; 
			try{
				iS = connection.getInputStream();
			}catch(IOException e){
				return;  
			}
			BufferedReader rD = new BufferedReader(new InputStreamReader(iS)) ; 
			String line ; 
			StringBuffer response = new StringBuffer() ; 
			while((line = rD.readLine())!= null){
				response.append(line) ; 
				response.append('\r') ; 
			}
			rD.close();
			responseStr = response.toString(); 
			
		}catch(Exception e ){
			e.printStackTrace() ; 
			return ; 
			
		}finally{
			
			if( connection != null){
				connection.disconnect(); 
			}
		}	
	public String getResponse( ){
	
		return responseStr; 
	}
	
	
	
	}
	


}