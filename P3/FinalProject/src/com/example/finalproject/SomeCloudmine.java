//Methods by Morgan W. 
//Put into object taking keys and Context as argument by Brandon R. 

package com.example.finalproject ; 



import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.cloudmine.api.rest.response.ObjectModificationResponse;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;


public class SomeCloudmine{
	private static String app_ID ; 
	private static String API_key ;
	private int airQualLength;
	public Context context ; 
	private ArrayList<WeatherObj> finalForecast; 
	public MainActivity activity ;
	public ArrayAdapter<String> adapter ;
	
	public SomeCloudmine(MainActivity activity, ArrayAdapter<String> adapter, String app_ID_, String API_key_ , int airQualLength_, Context context_ ){
		context = context_ ; 
		
		app_ID = app_ID_ ; 
		API_key = API_key_ ; 
		airQualLength = airQualLength_ ; 
		CMApiCredentials.initialize(app_ID, API_key, context);
		this.activity = activity ;
		this.adapter = adapter ;
	
	}

    public void parseAndSaveResults (String [][] tempWind, String [] qual)
    {
        int i, j ;
        airQualLength = qual.length ;
        //Log.d("CloudmineDebug", "Nothing in forecast: " + forecast) ;
        for (i = 0; i < qual.length; i++) //should loop 4 times (indices 0-3 of tempWind)
        {
            WeatherObj weatherObj = new WeatherObj(Integer.valueOf(tempWind[i][0]), Integer.valueOf(tempWind[i][1]), tempWind[i][2], tempWind[i][3], new AirNowObj(qual[0])) ;
            finalForecast.add(weatherObj) ;
            //Log.d("CloudmineDebug", "objects in forecast: " + forecast) ;
        }
        saveObjects(finalForecast) ;
        for (j = qual.length; j < tempWind.length; j++) //should loop 6 times (indices 4-9 of tempWind)
        {
            //Air quality will be given "null" placeholder to indicate that data has not been provided
            //This will be updated in compare
            WeatherObj weatherObj = new WeatherObj(Integer.valueOf(tempWind[j][0]), Integer.valueOf(tempWind[j][1]), tempWind[j][2], tempWind[j][3], new AirNowObj("null")) ;
            finalForecast.add(weatherObj) ;
        }
        //Log.d("CloudmineDebug", "Forecast: " + forecast) ;
    }

    //Save array of parsed items to Cloudmine
    public void saveObjects (ArrayList<WeatherObj> items)
    {
        LocallySavableCMObject.saveObjects
                (context,
                items,
                new Response.Listener<ObjectModificationResponse>()
                {
                    @Override
                    public void onResponse(ObjectModificationResponse objectModificationResponse)
                    {
                        switch (objectModificationResponse.getStatusCode())
                        {
                            case 200:
                                Log.d("CloudmineDebug", "Success") ;
                                Log.d("CloudmineDebug", "Objects were saved: " + objectModificationResponse.getCreatedObjectIds());
                                break ;
                            case 400:
                                Log.d("CloudmineDebug", "Invalid JSON request") ;
                                break ;
                            case 401:
                                Log.d("CloudmineDebug", "Incorrect API") ;
                                break ;
                            case 404:
                                Log.d("CloudmineDebug", "App ID not found") ;
                                break ;
                            default:
                                Log.d("CloudmineDebug", "Unknown response code: " + objectModificationResponse.getResponseCode()) ;
                                break ;
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("CloudmineDebug", "Error " + volleyError.getMessage()) ;
                    }
                }) ;
    }

    //Fix WeatherObjs missing AirNow data
    //Treat this like an asynctask in Android
    //Further processing after a call happens within onResponse (similar to onPostExecute)
    public void findObj ()
    {
        int i ;
        for (i = airQualLength; i < finalForecast.size(); i++) //should loop 6 times for indices 4-9
        {
            final int temp = finalForecast.get(i).temp ;
            final int wind = finalForecast.get(i).wind ;
            String loc = finalForecast.get(i).location ;
            final int passable = i ;
            //Log.d("CloudmineDebug", "For item: " + i + " with temp: " + temp + ", wind: " + wind) ;
            //for each: look up Cloudmine objects at SAME location with SIMILAR temp(+-20)/wind(+-10), grab airquality data, save
            //
            String search = SearchQuery.filter(WeatherObj.class).and("location").equal(loc).and("temp").greaterThanOrEqual(temp - 20).and("temp").lessThanOrEqual(temp + 20).and("wind").greaterThanOrEqual(wind - 10).and("wind").lessThanOrEqual(wind + 10).searchQuery() ;
            //Log.d("CloudmineDebug", "Search Query: " + search);
           // LocallySavableCMObject.searchObjects
            
            LocallySavableCMObject.searchObjects(this.context,
                    search,
                    new Response.Listener<CMObjectResponse>()
                    {
                        @Override
                        public void onResponse(CMObjectResponse response) {
                            //Log.d("CloudmineDebug", "Inside onResponse");
                            String found;
                            //Log.d("CloudmineDebug", "Response code: " + response.getResponseCode());
                            switch (response.getStatusCode()) {
                                case 200:
                                    found = response.getMessageBody(); //global
                                    Log.d("CloudmineDebug", "Found set to: " + found);
                                    //process returned items, and adjust array data
                                    //Log.d("CloudmineDebug", "Initial temp for index " + passable + " was: " + temp) ;
                                    JsonParser jp = new JsonParser() ;
                                    JsonElement jsonElement = jp.parse(found) ;
                                    JsonObject root = jsonElement.getAsJsonObject() ;
                                    JsonObject results = root.getAsJsonObject("success");
                                    try
                                    {
                                        Set<Map.Entry<String, JsonElement>> entries = results.entrySet();
                                        double testT, testW, bestT = 100, bestW = 100 ;
                                        JsonObject obj ;
                                        String bestKey = "" ;
                                        Log.d("CloudmineDebug", "Initial temp: " + temp);
                                        Log.d("CloudmineDebug", "Initial wind: " + wind);

                                        for (Map.Entry<String, JsonElement> entry: entries)
                                        {
                                            obj = results.getAsJsonObject(entry.getKey()) ;
                                            //Log.d("CloudmineDebug", "Obj: " + obj);
                                            int matchT, matchW ;
                                            matchT = obj.get("temp").getAsInt() ;
                                            matchW = obj.get("wind").getAsInt() ;

                                            Log.d("CloudmineDebug", "Key: " + entry.getKey());
                                            Log.d("CloudmineDebug", "Temp: " + matchT);
                                            if (temp != 0)
                                                testT = compare(temp, matchT);
                                            else
                                                testT = compareZ(temp, matchT);
                                            Log.d("CloudmineDebug", "Wind: " + matchW);
                                            if (wind != 0)
                                                testW = compare(wind, matchW);
                                            else
                                                testW = compareZ(wind, matchW);
                                            //Log.d("CloudmineDebug", "Checking " + testT + "<" + bestT + " && " + testW + "<" + bestW) ;

                                            if (testT < bestT && testW < bestW) //first entry should always set
                                            {
                                                bestKey = entry.getKey();
                                                bestT = testT ;
                                                bestW = testW ;
                                                //Log.d("CloudmineDebug", "Temp bestkey: " + bestKey) ;
                                            }
                                        }
                                        obj = results.getAsJsonObject(bestKey) ;
                                        Log.d("CloudmineDebug", "Bestkey: " + bestKey + " qual: " + obj.get("qual").getAsJsonObject().get("airQual").getAsString()) ;
                                        Log.d("CloudmineDebug", "Changing airQual: " + finalForecast.get(passable));
                                        finalForecast.get(passable).setQual(new AirNowObj(obj.get("qual").getAsJsonObject().get("airQual").getAsString()));
                                     
                                       
                                      // able).setQual(new AirNowObj(obj.get("qual").getAsJsonObject().get("airQual").getAsString()) );
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.d("CloudmineDebug", "Exception: " + ex.getMessage());
                                    }
                                    //TODO: do further components here (update UI, etc.)
                                    String[] forecastStrings = new String[finalForecast.size()] ; 
                        			for(int i =0; i < finalForecast.size();i++){
                        				
                        				
                        				forecastStrings[i] = finalForecast.get(i).getAsString() ;
                        			
                        				//eT.setText( forecastStrings[i]);
                        			}
                        			
                        			adapter.addAll(forecastStrings);
                                    
                                    break;
                                case 400:
                                    Log.d("CloudmineDebug", "Invalid JSON request");
                                    break;
                                case 401:
                                    Log.d("CloudmineDebug", "Incorrect API");
                                    break;
                                case 404:
                                    Log.d("CloudmineDebug", "App ID not found");
                                    break;
                                default:
                                    Log.d("CloudmineDebug", "Unknown response code: " + response.getResponseCode());
                                    break;
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Log.d("CloudmineDebug", "Error " + volleyError.getMessage()) ;
                        }
                    }
            );
        }
    }

    public double compare (int actual, int match)
    {
        double a = Math.abs((double) actual - (double) match) / actual * 100.0 ;
        Log.d("CloudmineDebug", "Percent error: " + a) ;
        return a ;
    }

    public double compareZ (int actual, int match)
    {
        double a = 2*(Math.abs((double) actual - (double) match) / Math.abs((double) actual + (double) match)) ;
        Log.d("CloudmineDebug", "Percent error (zero): " + a) ;
        return a ;
    }

  
}