//By Morgan W.  
//Edited by Brandon R. for getAsString() 
package com.example.finalproject ; 


/**
 * Created by cricket on 3/10/16.
 */
public class AirNowObj {
    public String airQual ;

    public AirNowObj(String airQual)
    {
        this.airQual = airQual ;
    }

    public AirNowObj()
    {
        //default
    }
    
    public void setAirQual (String airQual)
    {
        this.airQual = airQual ;
    }
    public String getAsString(){
    	
    	return airQual ; 
    }
}
