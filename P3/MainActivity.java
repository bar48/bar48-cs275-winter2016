//By Brandon R. 

package com.example.finalproject;


import java.net.HttpURLConnection;
import java.net.URL;


import android.app.Activity;
import android.content.Context;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;



public class MainActivity extends Activity implements OnClickListener{
	private static URL url1 ; 
	private static HttpURLConnection request1;

	private static AirNow aNow = null ;
	private static Forcast10 fTen = null ; 
	private static WeatherObj wObj = null ; 
	private static SomeCloudmine sClo = null ;
	private static String[][] weatherStrings ; 
	private static String[] airNowStrings ; 
	private static Context mainContext = null ; 
	private static ArrayAdapter<String> adapt ; 
	private static ListView lV; 
	private static TextView tV = null ;
	private static EditText eT;  
	private static String displayString ="" ; 
	private static String forecastStrings[] ; 
	
	private static ArrayList<WeatherObj> forecast ;
	
	
	/**In the future have these entered somehow**/
	private static final String WUNDERGROUND_KEY = "e1ded7b168257e5c"; 
	private static final String AIRNOW_KEY = "5CFBB971-49BC-40ED-A107-A196725C4FD8"  ; 
	private static final String CLOUDMINE_KEY = "fc0ed0c346e6453e9cb1c58024f319c6" ; 
	private static final String CLOUDMINE_ID = "fa194747e81c79485f0a9ec8582e3712" ; 
	
	public void setTextView( int id, String content ){
		tV = (TextView)this.findViewById( id ) ; 
		tV.setText( content );
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mainContext = this ; 
		lV = (ListView)this.findViewById( R.id.listView1); 
	
		DoStuff dS = new DoStuff() ; 
		dS.execute() ; 

		


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private class DoStuff extends AsyncTask<Void, Void, Void>{
		@Override
		protected Void doInBackground( Void... params){
			try{
				fTen = new Forcast10(WUNDERGROUND_KEY) ; 
				aNow = new AirNow( WUNDERGROUND_KEY, AIRNOW_KEY) ; 
				weatherStrings = fTen.getweather(); 
				airNowStrings = aNow.getAQI() ; 
				sClo = new SomeCloudmine( CLOUDMINE_ID, CLOUDMINE_KEY, 4, mainContext ) ;
				
				//SomeCloudmine( String app_ID_, String API_key_ , int airQualLength_, Context context_ ){
				
				
			}catch(Exception e){
				return null ; 
			}
			
			return null;
			
		}
		
		protected void onPostExecute(Void v){
			forecast = sClo.parseAndSaveResults( weatherStrings, airNowStrings) ; 
			sClo.findObj( forecast );
			
			forecastStrings = new String[forecast.size()] ; 
			for(int i =0; i < forecast.size();i++){
				
				
				forecastStrings[i] = forecast.get(i).getAsString() ;
			
				//eT.setText( forecastStrings[i]);
			}
			adapt = new ArrayAdapter<String>( mainContext, android.R.layout.simple_list_item_1, forecastStrings);
			
			lV.setAdapter(adapt);
			
			
		}
	}
	



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		DoStuff dS = new DoStuff() ; 
		dS.execute() ; 
		
	}
	

	
}
