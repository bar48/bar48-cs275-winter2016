//By Patrick N. 
//Ed. Brandon R. 

package com.example.finalproject ; 

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
//e1ded7b168257e5c
public class Forcast10 {
	private String mycity;
	private String state;
	private String[][]info = new String[10][4];
	private String city1;
	
	private String key ; 
	
	
	public Forcast10(String key_)throws MalformedURLException, IOException //throws two exceptions
	{
		URL url1;
		key = key_ ; 
        InputStream is = null;
        

        try 
        {     
        	//Edited by Brandon R. new url string
        	url1 = new URL( "http://api.wunderground.com/api/" + key +"/geolookup/q/autoip.json") ;
            //url1 = new URL("http://api.wunderground.com/api/e1ded7b168257e5c/conditions/q/autoip.json"); //new URL("https://www.cs.drexel.edu/~augenbdh/cs275_wi16/labs/wxunderground.html");
            //autoip.json
            HttpURLConnection request1 = (HttpURLConnection) url1.openConnection();
            request1.setInstanceFollowRedirects(false);
            request1.connect();           
             JsonParser jp1 = new JsonParser();
             JsonElement root1 = jp1.parse(new InputStreamReader((InputStream)request1.getContent()));
             //Edited by Brandon R. New query and struct
             JsonObject rootobj1 = root1.getAsJsonObject().get("location").getAsJsonObject(); //raw data  
             //Edited by Brandon R. New struct 
             String city = rootobj1.get("city").getAsString();//get city from the 3rd level
             //Edited by Brandon R. Commented out needless
             //city1 = city;
             //getcity(city);
             //Edited by Brandon R. New struct                      
             state = rootobj1.get("state").getAsString();//get state from the 3rd level
             //Edited by Brandon R. New url string 
             URL url = new URL("http://api.wunderground.com/api/"+key+"/forecast10day/q/"+state+"/"+city+".json");

     		HttpURLConnection request = (HttpURLConnection) url.openConnection();
             request.connect(); 
             request.setInstanceFollowRedirects( false);
             JsonParser jp = new JsonParser();
             JsonElement root = jp.parse(new InputStreamReader((InputStream)request.getContent()));               
             JsonObject rootobj = root.getAsJsonObject(); //raw data
             JsonObject forecast = rootobj.getAsJsonObject("forecast");
             JsonObject forecast1 = forecast.getAsJsonObject("simpleforecast"); 
             JsonArray forecast2 = forecast1.getAsJsonArray("forecastday");
             
             
             for(int i = 0; i < forecast2.size(); i++)
             {
             	String day = (String) forecast2.get(i).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString();
             	String month = (String) forecast2.get(i).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString();
             	String year = (String) forecast2.get(i).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
             	String date = month + "/" + day + "/" + year;
             	String temp = (String) forecast2.get(i).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
             	String maxwind = (String) forecast2.get(i).getAsJsonObject().get("maxwind").getAsJsonObject().get("mph").getAsString();   	
             	
             	for(int j = 0; j < 4; j++)
             	{
             		if(j == 0)
             			info[i][j] = temp;
             		if(j == 1)
             			info[i][j] = maxwind;
             		if(j == 2)
             			info[i][j] = date;
             		if(j == 3)
             			info[i][j] = city1;
             	}

             }
            
  
           
        } 
        catch (MalformedURLException mue) 
        {
            mue.printStackTrace();
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
        } 
        finally 
        {
            try 
            {
                if (is != null) is.close();
            } 
            catch (IOException ioe) 
            {
                // nothing to see here
            }
        }
	}
	public String getcity(String city)
	{
		for (char c : city.toCharArray()) 
        {
       	    if (Character.isWhitespace(c)) {
       	    	String[] tempc = city.split(" ");
       	    	mycity = tempc[0] + "_" + tempc[1];
       	    	return mycity;

       	    }
       	    
       	    
       	    
        }
   	   return mycity = city;
		
	}
	public String getnewcity()
	{
		return mycity;
	}
	public String getstate()
	{
		return state;
	}
	public String [][]getweather() throws IOException
	{
		return info;
	}

}
