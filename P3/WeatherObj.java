//By Morgan W.
// getAsString method by Brandon Rupert

package com.example.finalproject ; 

import com.cloudmine.api.db.LocallySavableCMObject;

/**
 * Created by cricket on 3/10/16.
 */
public class WeatherObj extends LocallySavableCMObject
{
    public int temp ;
    public int wind ;
    public String date ;
    public String location ;
    public AirNowObj qual ;

    public WeatherObj (int temp, int wind, String date, String loc, AirNowObj qual)
    {
        this.temp = temp ;
        this.wind = wind ;
        this.qual = qual ;
        this.location = loc ;
        this.date = date ;
    }

    public WeatherObj()
    {
        //default
    }

    public void setDate (String date)
    {
        this.date = date ;
    }

    public void setLocation (String location)
    {
        this.location = location ;
    }

    public void setQual (AirNowObj qual)
    {
        this.qual = qual ;
    }

    public void setTemp (int temp)
    {
        this.temp = temp ;
    }

    public void setWind (int wind)
    {
        this.wind = wind ;
    }
    //getAsString() by Brandon R. 
    public String getAsString(){
    	
    	return date + " " + location + " " + qual.getAsString() + " " + temp + " " + wind ; 
    }
}
