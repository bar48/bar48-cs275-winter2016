//By Patrick N. 
//Ed. Brandon R. -- key passed to constructor

package com.example.finalproject ; 

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class AirNow {
	private String zipcode;
	private String []items;
	private static String wKey ; 
	private static String aKey ; 

	public AirNow ( String wKey_, String aKey_) throws JsonIOException, JsonSyntaxException, IOException
	{
		//URL url1 = new URL("http://api.wunderground.com/api/e1ded7b168257e5c/conditions/q/autoip.json"); //new URL("https://www.cs.drexel.edu/~augenbdh/cs275_wi16/labs/wxunderground.html");
        //Edit by Brandon R different url string
		aKey = aKey_ ; 
		wKey = wKey_ ; 
		URL url1 = new URL( "http://api.wunderground.com/api/"+wKey+"/geolookup/q/autoip.json") ; 
        HttpURLConnection request1 = (HttpURLConnection) url1.openConnection();
        request1.connect();

       
         JsonParser jp1 = new JsonParser();
         JsonElement root1 = jp1.parse(new InputStreamReader((InputStream)request1.getContent()));   
         //Edit by Brandon R. different query 
         JsonObject rootobj1 = root1.getAsJsonObject().get("location").getAsJsonObject(); //raw data 
		
		Date todaysDate = new Date();
		URL url;
        InputStream is = null;
        //zipcode = rootobj1.get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("zip").getAsString();
       //Edit by Brandon R. different query 
        zipcode = rootobj1.get("zip").getAsString(); 
        //System.out.println(zipcode);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        
        String tempdate = df.format(todaysDate);
        String []tempd = tempdate.split("/");
        String currentdate = tempd[2] + "-" + tempd[1] + "-" + tempd[0];
  
  
                       

        try 
        {
            
            
            url = new URL("http://www.airnowapi.org/aq/forecast/zipCode/?format=application/json&zipCode="+zipcode+"&date="+currentdate+"&distance=25&API_KEY="+aKey);
            
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

           
             JsonParser jp = new JsonParser();
             JsonElement root = jp.parse(new InputStreamReader((InputStream)request.getContent()));   
             
             JsonArray rootobj = root.getAsJsonArray(); //raw data 
             
             
             
             items = new String[rootobj.size()];
             for(int i = 0; i < rootobj.size(); i++)
             {
            	 String AQI = (String) rootobj.get(i).getAsJsonObject().get("AQI").getAsString();
            	 items[i] = AQI;
            	 
            	 //System.out.println(AQI);
            	 
             }
            
             
           
        } 
        catch (MalformedURLException mue) 
        {
            mue.printStackTrace();
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
        } 
        finally 
        {
            try 
            {
                if (is != null) is.close();
            } 
            catch (IOException ioe) 
            {
                // nothing to see here
            }
        }

		
	}
	public String[] getAQI()
	{
		return items;
	}
	

}
