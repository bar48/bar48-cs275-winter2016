package com.example.lookaround ; 


public class LookUser{
	//EARTH_RADIUS in miles
	private static final double  EARTH_RADIUS = 3960.00 ; // in miles 
	private static double degsLat, degsLong ; 
	private static double secondsEpoch ; 
	String userName, email ; 
	LookUser( double degsLat_, double degsLong_, String userName_, String email_, double seconds_){
		degsLat = degsLat_ ; 
		degsLong = degsLong_ ; 
		userName = userName_ ; 
		email = email_ ; 
		secondsEpoch = seconds_ ; 
	}
	public  double degToRad( double deg ){
		return ( deg*Math.PI/180.0 )  ; 

	}
	//Use haversine, note http://andrew.hedges.name/experiments/haversine/
	public double getMilesSeparationFrom( LookUser someoneElse  ){
		double lat2 = degsLat ; 
		double lat1 = someoneElse.getDegsLat() ; 
		double long2 = degsLong ; 
		double long1 = someoneElse.getDegsLong() ;  
		double dLat = degToRad( lat2 - lat1 )  ; 
		double dLong = degToRad( long2 - long1 )  ; 
		double a =  Math.pow( Math.sin(dLat/2.0) , 2.0 )    + ( Math.cos(lat1)*Math.cos(lat2)*Math.pow( dLong/2.0 , 2.0 )      ) ; 
		double c = 2*Math.atan2(  Math.pow( a, 0.5    ) , Math.pow( 1-a, 0.5 )  )  ; 
		return  EARTH_RADIUS*c ; 

	}
	public double getSecondsSeparationFrom( LookUser someoneElse){
		double diff = someoneElse.getSecondsEpoch() - secondsEpoch ; 
		if ( diff < 0.0 ) diff*=(-1);
		return diff ; 
	}
	//coords intended for google maps
	public double getDegsLat(){
		return degsLat ; 
		
	}
	public double getDegsLong(){
	
		return degsLong ; 
	}
	//for contact
	public String getEmail(){
		
		return email ; 
	}
	public String getUserName(){
		
		return userName ; 
	}
	public double getSecondsEpoch(){
		return secondsEpoch ; 
	}
	public boolean isWithinMilesOf(  LookUser someoneElse , double miles  ){
	
		return (  getMilesSeparationFrom( someoneElse ) <= miles ) ; 
	}
	//Good line for displaying in a scrolling list
	public String getInfo(){
		
		
		return userName + " <" + email + ">" + degsLat + " " + degsLong ; 
	}





}
