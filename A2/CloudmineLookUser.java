package com.example.lookaround ; 

import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet.ObjectSetInputSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectSet.ObjectSetResultSet;

import com.temboo.core.TembooSession;
//extended LookUser with capability of writing to Cloudmine 
//Uses Temboo
public class CloudmineLookUser extends LookUser{
	private static TembooSession tSession ; 
	private static ObjectSet oSet ; 
	private static ObjectSetInputSet oiSet ;
	private static ObjectSetResultSet orSet ; 
	

	CloudmineLookUser(double degsLat_, double degsLong_, String userName_, String email_, 
			double seconds_) {
		super(degsLat_, degsLong_, userName_, email_, seconds_);
	
	}
	


	private String quoted( String stringIn){
		
		return "\"" + stringIn + "\"" ; 
	}
	//Enter parameters for temboo operations. acctName, appKeyName, and appKeyVal from temboo, 
	//CMapiKey and CMappID from CLoudmine
	public void enter(String acctName,  String appKeyName, String appKeyVal, String CMapiKey,
				String CMappID) throws Exception{
		tSession = new TembooSession(acctName, appKeyName, appKeyVal ) ; 
		oSet = new ObjectSet(tSession) ; 
		oiSet = oSet.newInputSet(); 
		String dataInput = "{" + quoted( "" + this.getSecondsEpoch()) + ":{" + quoted( "userName") + ":"
				+ quoted( this.getUserName() ) + "," + quoted( "email") + ":" 
				+ quoted(this.getEmail() ) + ", " + quoted("degsLat") + ":"
				+ quoted(""+this.getDegsLat() ) + ", " + quoted( "degsLong") + ":"
				+ quoted(""+ this.getDegsLong() ) + ", " + quoted( "secondsEpoch") + ":"
				+ quoted(""+this.getSecondsEpoch()) +"}}";
		//System.out.println(dataInput);
		oiSet.set_Data(dataInput);
		oiSet.set_APIKey( CMapiKey );
		oiSet.set_ApplicationIdentifier( CMappID);
		try{
			orSet = oSet.execute( oiSet) ; 
		}catch(Exception e ){
			//e.printStackTrace(); 
			return ; 
		}
		
		
		
		
	}

}
