package com.example.lookaround ; 

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;


import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet.ObjectGetInputSet;
import com.temboo.Library.CloudMine.ObjectStorage.ObjectGet.ObjectGetResultSet;

import com.temboo.core.TembooSession;

public class CloudmineQueryAll{
	private static String acctName, appKeyName, appKeyVal, CMapiKey, CMappID ; 
	private static TembooSession tSession ; 
	private static ObjectGet oGet ; 
	private static ObjectGetInputSet oiGet ;
	private static ObjectGetResultSet orGet ; 
	private static LookUser[] queryResults = null ; 
	CloudmineQueryAll(String _acctName,  String _appKeyName, String _appKeyVal, String _CMapiKey,
				String _CMappID) throws Exception{
		acctName = _acctName ; 
		appKeyName= _appKeyName ; 
		appKeyVal = _appKeyVal ; 
		CMapiKey = _CMapiKey ; 
		CMappID = _CMappID ; 
		tSession = new TembooSession(acctName, appKeyName, appKeyVal ) ; 
		oGet = new ObjectGet(tSession) ; 
		oiGet = oGet.newInputSet(); 
	
		//System.out.println(dataInput);
		oiGet.set_APIKey( CMapiKey );
		oiGet.set_ApplicationIdentifier( CMappID);
		try{
			orGet = oGet.execute( oiGet) ;
			//System.out.println( orGet.get_Response() );
			JSONObject results = new JSONObject( orGet.get_Response());
			JSONObject success = new JSONObject( results.get("success").toString()); 
			JSONObject temp ; 
			
			//int sizeSuccess = success.keySet().size();
			Iterator<String> iter= success.keys(); 
			List<String> keyList = new ArrayList<String>() ; 
			while ( success.keys().hasNext()) keyList.add(iter.next());
			int sizeSuccess = keyList.size(); 
			//Object[] keys = success.keySet().toArray( new String[sizeSuccess]); 
			queryResults = new LookUser[sizeSuccess]; 
			double degsLat , degsLong , secondsEpoch; 
			String userName, email ; 
			for( int i = 0 ; i < sizeSuccess; i++){
				temp = success.getJSONObject( keyList.get(i)) ;
				secondsEpoch= Double.parseDouble( temp.get("secondsEpoch").toString()) ;
				degsLat = Double.parseDouble(temp.get("degsLat").toString()) ; 
				degsLong = Double.parseDouble(temp.get("degsLong").toString() ) ; 
				userName = temp.get("userName").toString() ; 
				email = temp.get("email").toString() ;
				queryResults[i] = new LookUser( degsLat, degsLong, userName, email, secondsEpoch);
				//degsLong = temp.get("degsLong") ; 
				//System.out.println(degsLong);
			}
			
/**
 * 
 * 
 * 	private static double degsLat, degsLong ; 
	private static double secondsEpoch ; 
	String userName, email ; 			
 */
			
			
		}catch(Exception e){
			return ; 
		}
		
	}
	//Return all
	public List<LookUser> getAllQueries(){
		if( queryResults.length == 0 ) return null ; 
		List<LookUser> general = new ArrayList<LookUser>(); 
		for( int i =0; i <queryResults.length;i++){
			general.add( queryResults[i]); 
		}
		return general ; 
	}
	//Return only those within some range of miles 
	public List<LookUser> getWithinMilesTime( double distance,double seconds, LookUser myself){
		List<LookUser> specific = new ArrayList<LookUser>() ;
		for (int i =0; i < queryResults.length; i++){
			if ( myself.isWithinMilesOf( queryResults[i], distance) && (queryResults[i].getSecondsSeparationFrom(myself) < seconds)){
				specific.add( queryResults[i]);
			}
		}
		return specific ; 
	}
	

}
