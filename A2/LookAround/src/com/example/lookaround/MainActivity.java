package com.example.lookaround;


import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Handler ; 
import android.os.Bundle ; 

public class MainActivity extends Activity {
	private static  final double MILES = 600.0  ; 
	private static double epochSecondsNow = 0.0 ; 
	private static int counter = 1 ; 
	private static final double MINS_INTERVAL = 0.12; 
	private static TextView currentTV;
	private static final String MY_USER_NAME = "BrandonRupert";
	private static final String EMAIL = "barupert@gmail.com" ; 
	private static LocationManager loMan ; 
	private static Location lo ;
	private static double longitude ; 
	private static double latitude ; 
	private static CloudmineLookUser myself  ; 
	private static CloudmineQueryAll query ; 
	private static LookUser myselfLook ; 
	private static List<LookUser> populatingList;
	private ListView nearbyList ; 
	private static String[] infoArray ; 
	private  ArrayAdapter adapt ; 
	private static ArrayList<String> stringArrayList ; 
	//as many milliseconds as there are in 5 minutes:
	private static final long INTERVAL = (long) (MINS_INTERVAL*60.0*1000.0); 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final Handler hand = new Handler() ;
		hand.postDelayed( new Runnable(){

			@Override
			public void run() {
				updateListView(); 
				
				hand.postDelayed(this, INTERVAL);
				
			}}, INTERVAL);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//ASYNC BELOW
	
	public void updateListView( ){
		loMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE) ; 
		lo = loMan.getLastKnownLocation(LocationManager.GPS_PROVIDER) ; 
		longitude = lo.getLongitude(); 
		latitude = lo.getLatitude(); 
		epochSecondsNow = System.currentTimeMillis() ; 
		myself = new CloudmineLookUser(latitude, longitude,  MY_USER_NAME, EMAIL,epochSecondsNow );
		myselfLook = (LookUser) myself; 
		try{ 
			
			query = new CloudmineQueryAll( "bar48", "LookAround", "LYEubTWfHVcaqgVfLdzc6d4PwzM9PeLB", 
				"4bb20bd5915a47bdae0943d54c9ab82e","d3ae1d062671c44e6f6bc472940f5ba9" );
			
			populatingList = query.getWithinMilesTime( MILES, (double)10000,  myselfLook ) ;
			
		}catch( Exception e){
			
			return ; 
		}
		//build array lists to populate list view 
		try{ 
			nearbyList = (ListView) findViewById(R.id.nearbyList); 
			infoArray = new String[ populatingList.size() ] ; 
			for( int i =0; i < infoArray.length; i++){
				
				infoArray[i] = populatingList.get(i).getInfo(); 
			}
			
			for ( int i =0; i < infoArray.length; i++){
				stringArrayList.add( infoArray[i]); 
			}
			
		}catch( Exception e){
			return ;  
		}
		
		adapt = new ArrayAdapter<String>(this, R.id.nearbyList, infoArray);
		nearbyList.setAdapter(adapt);
	}
	
	
	
}
