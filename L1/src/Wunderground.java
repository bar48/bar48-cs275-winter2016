import java.util.Scanner ;  
import com.google.gson.JsonElement  ; 
import com.google.gson.JsonObject ;
import com.google.gson.JsonArray ; 

/**
 * Uses RootJsonObj
 * @author Brandon Rupert
 *
 */
public class Wunderground{
	
	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main (String[] args) throws Exception{
		
		//For URLs and for values to be printed 
		String key, autoIP, cityName, stateAbbrev, latit, longit, zip,
			hourly, pretty, weekdayName, condit, tempF, relHumidity ; 
	
		//
		RootJsonObj ro; 
		JsonObject loc , fctTime , fctObj ; 
		JsonArray hourlyFct ; 

		
		if ( args.length < 1 ){
			System.out.print("Enter key : " );
			Scanner keyPrompt = new Scanner(System.in) ; 
			key = keyPrompt.nextLine() ; 
			keyPrompt.close(); 
		}
		else key = args[0] ; 
		key = key.trim(); 
		
		//I propose to use autoIP to get location info
		autoIP ="http://api.wunderground.com/api/"
				+key+"/geolookup/q/autoip.json" ;
		ro = new RootJsonObj(autoIP) ; 
		try {
			loc = ro.getJsonObject().get("location").getAsJsonObject() ; 
		}
		catch( NullPointerException e){
			System.err.println("Could not access. Exiting. Check key") ; 
			return; 
		}
		cityName = loc.get("city").getAsString(); 
		stateAbbrev = loc.get("state").getAsString(); 
		latit = loc.get("lat").getAsString(); 
		longit = loc.get("lon").getAsString(); 
		zip = loc.get("zip").getAsString(); 
		System.out.println( cityName +" , " + stateAbbrev +" LAT " +latit 
				+ " LON " +longit + " ZIP " + zip )  ;
	
		//For query of hourly weather forecast 
		hourly = "http://api.wunderground.com/api/"+key+"/hourly/q/"+stateAbbrev+"/"
				+cityName +".json" ; 
		ro = new RootJsonObj(hourly) ; 

		//rootObj = root.getAsJsonObject(); 
		try{
			hourlyFct = ro.getJsonObject().get("hourly_forecast").getAsJsonArray();
		}
		catch( NullPointerException e){
			
			System.err.println("Could not access. Exiting Check Key");
			return ; 
		}
		
		
		//JsonArray is collection of JsonElements, iterate...
		for(  JsonElement eachFct : hourlyFct){
			fctObj = eachFct.getAsJsonObject();
			fctTime = fctObj.get("FCTTIME").getAsJsonObject(); 
			pretty = fctTime.get("pretty").getAsString(); 
			weekdayName = fctTime.get("weekday_name").getAsString(); 
			
			condit = fctObj.get("condition").getAsString().toLowerCase(); 
			tempF = fctObj.get("temp").getAsJsonObject().get("english").getAsString() ; 
			relHumidity = fctObj.get("humidity").getAsString(); 
			System.out.println( "For " + weekdayName + ", " + pretty + ", it is " + tempF + " degrees F" +
									" and " + condit + " with rel humidity " + relHumidity +".");
		}
		
				
	}
}
