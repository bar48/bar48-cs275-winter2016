package com.example.tictactoe;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	//start new game of tictactoe. 
	// initialize entered character 
	// declare outcome TextView and current Button
	static private Tictactoe game ; 
	static private char charIn = 'O';  //intialized, changed with turn
	TextView outcome ; 
	Button current ; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		game = new Tictactoe() ; 
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	// do nothing if game won or tied 
	// get current button 
	//get resource name of clicked button 
	// split resource name by underscores _ and parse last two elements of split as row and col ints
	// if entry corresp. to button, entry at row,col, is valid, update entered character and enter it, 
	// 				then change button's text 
	// check if tied or won, set message accordingly. 
	public void onClick(View v) {
		if ( game.isTied() || game.isWon() ) return; 
		current= (Button)v.findViewById(v.getId());
		String clickedString =  v.getResources().getResourceName(v.getId());
		String[] splitString = clickedString.split("_"); 

		int row = Integer.parseInt(splitString[1]) ; 
		int col = Integer.parseInt(splitString[2]); 
		//Clicking wrong button does not change turn 
		if ( game.isAvailable(row, col)){
			if ( charIn == 'X') charIn = 'O'; 
			else charIn = 'X'; 
			game.setCharAt( row, col,  charIn);
			current.setText(""+charIn);
		}
		
		
		if ( game.isTied() ){
			outcome = (TextView) this.findViewById( R.id.outcomeTextField); 
		
			outcome.setText("YOU TIED!!!");
		}
		else if( game.isWon()){
			
			outcome = (TextView) this.findViewById( R.id.outcomeTextField); 
	
			outcome.setText(charIn + " WON!!!");
			
		}
	}
}
