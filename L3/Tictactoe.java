package com.example.tictactoe;

public class Tictactoe{

	private static char[] grid ; 
	private static boolean gameWon ; 
	private static boolean gameTied ; 
	
	//Constructor 
	//9 elements in an array of character, all initial '\0' 
	//convert row and col in method args to corresponding index. 
	public Tictactoe( ){
	
		grid = new char[9] ; 
		
		gameWon = false ;   
		gameTied = false ; 

	}
	//Get index from row and col (1-3, 1-3)
	private int getIndex( int row, int col ){
		return ( ( (row - 1)*3 ) + ( col - 1 ) ) ; 
		
	
	}
	//If row and col are in bounds
	private boolean isRowColOkay(    int row, int col ){
	
		return ( (row >=1 ) && ( row <= 3) && ( col >=1 ) && ( col <= 3)  ) ; 
	}
	// if leftmost element in row equals center and rightmost, and if leftmost filled in 
	private boolean rowIsAllSame( int row ){
	
		return ( getCharAt(row,1) == getCharAt(row,2) ) && ( getCharAt(row,1) == getCharAt(row,3)) && (!isAvailable( row,1));  
	}
	// if top element in col equals center and bottom and if top is filled in 
	private boolean colIsAllSame( int col ){
	
		return ( getCharAt(1,col) == getCharAt(2,col) ) && ( getCharAt(1,col) == getCharAt(3,col)) && (!isAvailable(1,col));  
	}
	// 3x3 diagonal from top left to bottom right all identical, top left filled in 
	private boolean rightDiagonalSame( ){
	
		return ( getCharAt(1,1) == getCharAt(2,2) ) && ( getCharAt(1,1) == getCharAt(3,3)) && (!isAvailable( 1,1));   
	}
	//3x3 diagonal from bottom left to top right all identical , bottom left filled in 
	private boolean leftDiagonalSame( ){
		return ( getCharAt(3,1) == getCharAt(2,2) ) && ( getCharAt(3,1) == getCharAt(1,3)) && (!isAvailable( 3,1)); 
	}
	//if a space is available; has '\0'
	public boolean isAvailable( int row, int col ){
		if (  (row >=1 ) && ( row <= 3 ) && (  col >= 1 ) && ( col <= 3)){
			return getCharAt(row,col) == '\0' ; 
		}
		return false ; 
	}
	// check for row or cols filled in all the same, or diagonals. set gameWon true 
	private void checkWin(){
	
		for(int i =1; i <= 3; i++ ){
		
			if ( rowIsAllSame(i) || colIsAllSame(i) ){
				gameWon = true ;
				return ; 
			
			}

		}
		if (leftDiagonalSame() || rightDiagonalSame() ){
			
			gameWon =true ; 
		
		}
	
	
	
	}
	//if game is not won, and no unoccupied entries found, set gameTied true 
	private void checkTie(){
	
		for( int i =1 ; i<=3; i++){
		
		
			for( int j = 1; j <= 3; j++){
			
				if (isAvailable(i,j) ) return ; 
			
			}
		}
		if ( !gameWon ){
			gameTied = true ; 
	
			
		}
	
	
	
	}
	//get character at integer corresp. to row and col
	public char getCharAt( int row, int col ){
	
		if (!isRowColOkay(row,col)) return '\0';
		return grid[ getIndex( row, col ) ] ; 
	}
	//set char at...
	public void setCharAt( int row, int col,  char newChar){
		if ( isRowColOkay( row,col)  && ( newChar == 'X' || newChar == 'O' ) && !(gameWon || gameTied)){
			if ( isAvailable( row,col)){
				grid[ getIndex(row,col) ] = newChar ; 
			}
			
		}
		checkWin() ; 
		checkTie() ; 
		
		
	}
	//see if won 
	public boolean isWon(){
	
		return gameWon ; 
	}
	//see if tied. 
	public boolean isTied(){
	
		return gameTied ; 
	}
	//clear and restart 
	public void eraseAll( ){
		for( int i =0; i < grid.length; i++ ){
			grid[i] = '\0' ; 
		}
	
		gameWon = false ;  
		gameTied = false ; 
	}
	//System.out.print view of 3x3 board to test 
	public void showBoard(){
	
		String out = "" ; 
		char square ; 
		for( int i = 1; i <=3; i++){
			for( int j =1; j<=3; j++){
			
				square = getCharAt(i,j) ; 
				if ( square == '\0' ) out += " _ " ; 
				else out += " " + square + " " ; 
				
			}
			out += "\n" ; 
		
		
		}
		System.out.print( out ) ; 
	}
	

}